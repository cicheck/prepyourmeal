import 'dart:collection';
import 'package:intl/intl.dart';

import 'package:cookbook/models/ProductChooserModel.dart';
import 'package:cookbook/models/product_model.dart';
import 'package:cookbook/models/recipe_model.dart';
import 'package:cookbook/utils/calendar_util.dart';
import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:table_calendar/table_calendar.dart';

class RecipesProvider with ChangeNotifier {
  List<ProductModel> _products = []; // lista produktów
  List<RecipeModel> _recipes = []; // lista przepisów
  List<RecipeModel> _recentRecipes =
      []; // lista ostatnio przeczytanych przepisów
  late Future<Database> database; // basa danych (sqlite)
  Map<DateTime, List<RecipeModel>> _calendarMap = {}; // mapa kalendarza

  // gettery
  List<RecipeModel> get recipes => _recipes;
  List<RecipeModel> get recentRecipes => _recentRecipes;
  List<ProductModel> get products => _products;

  RecipesProvider() {
    // konstruktor otwiera lub tworzy bazę danych
    database = createDatabase();
    initDb();
  }

  /// Pobieranie produktów z bazy danych przy starcie aplikacji */
  Future<List<ProductModel>> getProducts() async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db.query('products');

    return List.generate(maps.length, (i) {
      return ProductModel(
        id: maps[i]['id'],
        carbohydrates: maps[i]['carbohydrates'],
        fat: maps[i]['fat'],
        protein: maps[i]['protein'],
        title: maps[i]['title'],
      );
    });
  }

  Future<void> getCalendarMapItems() async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps =
        await db.query('meals'); // pobieranie tabeli 'meals' z bazy danych
    for (var map in maps) {
      DateTime day =
          DateFormat("dd-MM-yyyy").parse(map["day"]); // ucinamy godziny z daty
      var recipe = _recipes.firstWhere((element) =>
          element.id == map['recipe_id']); // wyciągamy przepis o danym id
      if (_calendarMap.containsKey(day)) {
        _calendarMap[day]!.add(
            recipe); // dodajemy przepis do listy przepisów danego dnia jeżeli dany dzień już jest jako klucz
      } else {
        _calendarMap[day] = [
          recipe
        ]; // jak dany dzień jeszcze nie jest kluczem, to wrzucamy do niego listę zawierającą przepis
      }
    }
  }

  /// pobranie przepisów z bazy danych przy starcie aplikacji */
  Future<List<RecipeModel>> getRecipes() async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db.query('recipes');

    return List.generate(maps.length, (i) {
      return RecipeModel.fromJson(maps[i]);
    });
  }

  /// funkcja tworząca haszmapę - taka potrzeba do biblioteki z kalendarzem */
  LinkedHashMap<DateTime, List<RecipeModel>> getCalendarMap() {
    final kEvents = LinkedHashMap<DateTime, List<RecipeModel>>(
      equals: isSameDay,
      hashCode: getHashCode,
    )..addAll(_calendarMap);

    return kEvents;
  }

  /// Funkcja odpowiedzialna za dodanie przepisu do danego dnia i zapisanie go w bazie danych */
  Future<void> addCalendarMapItem(DateTime time, RecipeModel recipe) async {
    if (_calendarMap.containsKey(time)) {
      _calendarMap[time]!.add(recipe);
    } else {
      _calendarMap[time] = [recipe];
    }
    var newObj = {
      'day': DateFormat("dd-MM-yyyy").format(time).toString(),
      'recipe_id': recipe.id,
    };
    final Database db = await database;
    await db.insert(
      'meals',
      newObj,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    notifyListeners();
  }

  /// pobieranie wszystkich list z bazy danych */
  void initDb() async {
    _products = await getProducts();
    _recipes = await getRecipes();
    await getCalendarMapItems();
    notifyListeners();
  }

  /// Dodawanie przepisu do listy ostatnio przeglądanych przepisów */
  void addRecipeToRecents(RecipeModel recentRecipe) {
    _recentRecipes.removeWhere((element) =>
        element ==
        recentRecipe); // jeżeli ten przepis istnieje w liście, to go wyrzucamy
    _recentRecipes.insert(0, recentRecipe); // dodajemy ten przepis na samą górę
    notifyListeners();
  }

  /// szukanie przepisów */
  List<RecipeModel> searchRecipes(String value) => _recipes
      .where((r) => r.title.toLowerCase().contains(value.toLowerCase()))
      .toList();

  /// tworzenie/otwieranie bazy danych sqlite */
  Future<Database> createDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'foods_database.db'),
      onCreate: (db, version) async {
        // jak baza jest dopiero tworzona
        await db.execute(
          """CREATE TABLE products(id INTEGER PRIMARY KEY, title TEXT, protein INTEGER, fat INTEGER, carbohydrates INTEGER);
          """,
        );
        await db.execute(
            "CREATE TABLE recipes(id INTEGER PRIMARY KEY, title TEXT, image TEXT, products TEXT, portions INTEGER, minutes INTEGER, steps TEXT);");

        await db.execute(
            "CREATE TABLE meals(id INTEGER PRIMARY KEY, day TEXT, recipe_id INTEGER);");

        List<ProductModel> products = [
          new ProductModel(
            id: 1,
            carbohydrates: 0,
            fat: 4,
            protein: 31,
            title: "Chicken",
          ),
          new ProductModel(
            id: 2,
            carbohydrates: 0,
            fat: 20,
            protein: 26,
            title: "Beef",
          ),
          new ProductModel(
            id: 3,
            carbohydrates: 0,
            fat: 21,
            protein: 26,
            title: "Pork",
          ),
          new ProductModel(
            id: 4,
            carbohydrates: 0,
            fat: 15,
            protein: 20,
            title: "Makrela",
          ),
          new ProductModel(
            id: 5,
            carbohydrates: 0,
            fat: 12,
            protein: 22,
            title: "Salmon",
          ),
          new ProductModel(
            id: 6,
            carbohydrates: 77,
            fat: 1,
            protein: 3,
            title: "Rice",
          ),
          new ProductModel(
            id: 7,
            carbohydrates: 78,
            fat: 2,
            protein: 13,
            title: "Kasha",
          ),
          new ProductModel(
            id: 8,
            carbohydrates: 2,
            fat: 0,
            protein: 15,
            title: "Potato",
          ),
          new ProductModel(
            id: 9,
            carbohydrates: 0,
            fat: 81,
            protein: 1,
            title: "Butter",
          ),
          new ProductModel(
            id: 10,
            carbohydrates: 5,
            fat: 3,
            protein: 3,
            title: "Milk",
          ),
          new ProductModel(
            id: 11,
            carbohydrates: 3,
            fat: 33,
            protein: 23,
            title: "Cheese",
          ),
          new ProductModel(
            id: 12,
            carbohydrates: 5,
            fat: 3,
            protein: 4,
            title: "Yogurt",
          ),
          new ProductModel(
            id: 13,
            carbohydrates: 4,
            fat: 4,
            protein: 4,
            title: "Mozzarella",
          ),
          new ProductModel(
            id: 14,
            carbohydrates: 1,
            fat: 18,
            protein: 18,
            title: "Ham",
          ),
          new ProductModel(
            id: 15,
            carbohydrates: 4,
            fat: 0,
            protein: 1,
            title: "Tomato",
          ),
          new ProductModel(
            id: 16,
            carbohydrates: 8,
            fat: 0,
            protein: 1,
            title: "Strawberry",
          ),
          new ProductModel(
            id: 17,
            carbohydrates: 4,
            fat: 0,
            protein: 1,
            title: "Cucumber",
          ),
          new ProductModel(
            id: 18,
            carbohydrates: 9,
            fat: 0,
            protein: 1,
            title: "Carrot",
          ),
          new ProductModel(
            id: 19,
            carbohydrates: 3,
            fat: 0,
            protein: 1,
            title: "Celery",
          ),
          new ProductModel(
            id: 20,
            carbohydrates: 14,
            fat: 0,
            protein: 0,
            title: "Apple",
          ),
          new ProductModel(
            id: 21,
            carbohydrates: 23,
            fat: 0,
            protein: 1,
            title: "Banana",
          ),
          new ProductModel(
            id: 22,
            carbohydrates: 6,
            fat: 11,
            protein: 1,
            title: "Olive",
          ),
          new ProductModel(
            id: 23,
            carbohydrates: 0,
            fat: 0,
            protein: 0,
            title: "Salt",
          ),
          new ProductModel(
            id: 24,
            carbohydrates: 64,
            fat: 3,
            protein: 10,
            title: "Pepper",
          ),
        ];
        List<RecipeModel> recipes = [
          new RecipeModel(
              id: 1,
              image: Image.network(
                  "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/13385.jpg?output-format=auto&output-quality=auto"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Curry Chicken"),
          new RecipeModel(
              id: 2,
              image: Image.network(
                  "https://www.kwestiasmaku.com/sites/v123.kwestiasmaku.com/files/pierogi-ruskie-02_0.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Pierogi ruskie"),
          new RecipeModel(
              id: 3,
              image: Image.network(
                  "https://www.kwestiasmaku.com/sites/v123.kwestiasmaku.com/files/pizza_margherita02.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Pizza"),
          new RecipeModel(
              id: 4,
              image: Image.network(
                  "https://tesco.pl/imgglobal/content_platform/recipes/main/4a/sized/756x426-100-fff-700-0/4a0f704c386937ae72524bbae33e0630.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Juicy burger"),
          new RecipeModel(
              id: 5,
              image: Image.network(
                  "https://cdn.galleries.smcloud.net/t/galleries/gf-Ckx1-ewhk-2rn2_domowy-hamburger-wedlug-przepisu-magdy-gessler-z-kuchennych-rewolucji-w-kielcach-664x442-nocrop.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Burger by Magda Gessler"),
          new RecipeModel(
              id: 6,
              image: Image.network(
                  "https://simply-delicious-food.com/wp-content/uploads/2019/09/Shrimp-Aglio-Olio-4.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Shrimp pasta"),
          new RecipeModel(
              id: 7,
              image: Image.network(
                  "https://assets.tmecosys.com/image/upload/t_web767x639/img/recipe/ras/Assets/3d42e0d7-fff4-46cc-bb4e-7fec6cdf1961/Derivates/4fad1b24-6b27-43f8-90a7-c6e49bdc0688.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Japanese pancakes"),
          new RecipeModel(
              id: 8,
              image: Image.network(
                  "https://www.mojegotowanie.pl/uploads/media/recipe/0001/99/makaron-ze-szpinakiem.jpeg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Pasta with spinach"),
          new RecipeModel(
              id: 9,
              image: Image.network(
                  "https://www.servingdumplings.com/wp-content/uploads/2020/11/ramen-with-miso-roasted-pumpkin-2-97fb11ab.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Ramen"),
          new RecipeModel(
              id: 10,
              image: Image.network(
                  "https://preppykitchen.com/wp-content/uploads/2019/08/Pancakes-recipe-1200.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Panacakes"),
          new RecipeModel(
              id: 11,
              image: Image.network(
                  "https://kobietaistyl.pl/wp-content/uploads/2020/04/Fluffy-Pancakes-New-CMS1.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Fluffy American pancakes"),
          new RecipeModel(
              id: 12,
              image: Image.network(
                  "https://images.eatsmarter.com/sites/default/files/styles/max_size/public/grilled-carp-fillets-with-honey-and-soy-sauce-520938.jpg"),
              minutes: 5,
              portions: 4,
              products:
                  _products.map((e) => ProductChooserModel(e, 10)).toList(),
              steps: ["s"],
              title: "Grilled carp"),
        ];

        //wrzucamy początkowo jakieś wartości do bazy danych
        recipes.forEach((r) async => await db.insert('recipes', r.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace));
        products.forEach((p) async => await db.insert('products', p.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace));
      },
      version: 1,
    );
  }

  /// dodawanie produktów do bazy danych */
  Future<void> addProduct(ProductModel product) async {
    final Database db = await database;
    await db.insert(
      'products',
      product.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    _products.add(product);
    notifyListeners();
  }

  Future<void> addRecipe(RecipeModel recipe) async {
    final Database db = await database;
    await db.insert(
      'recipes',
      recipe.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    _recipes.add(recipe);
    notifyListeners();
  }
}
