import 'package:cookbook/screens/add_recipe_screen.dart';
import 'package:cookbook/screens/meal_planner_screen.dart';
import 'package:cookbook/screens/recipes_list_screen.dart';
import 'package:cookbook/screens/search_recipe_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// funkcja tworząca navbar w screenach aplikacji
/// id - id aktualnego screena */
AppBar createAppBar(
    {required BuildContext context,
    required String title,
    required int id,
    List<Widget> actions = const []}) {
  var labels = [
    {
      "title": "Add Recipe",
      "icon": (Icons.add),
      "routeName": AddRecipeScreen.routeName,
      "id": 1,
    },
    {
      "title": "Search recipes",
      "icon": (Icons.search),
      "routeName": SearchRecipeScreen.routeName,
      "id": 2,
    },
    {
      "title": "My recipes",
      "icon": (Icons.menu_book),
      "routeName": RecipesListScreen.routeName,
      "id": 3,
      "arguments": RecipesListScreen.myRecipes,
    },
    {
      "title": "Recent Recipes",
      "icon": (Icons.schedule_outlined),
      "routeName": RecipesListScreen.routeName,
      "id": 4,
      "arguments": RecipesListScreen.recentRecipes,
    },
    {
      "title": "Meal planner",
      "icon": (Icons.calendar_today),
      "routeName": MealPlannerScreen.routeName,
      "id": 5,
    },
  ].where((element) =>
      element["id"] != id); // nie bierzemy labela, w którym aktualnie jesteśmy
  return new AppBar(
      iconTheme: IconThemeData(
          color: Color.fromRGBO(212, 195, 176, 1)), // ustawiamy kolor
      backgroundColor: Color.fromRGBO(46, 42, 38, 1),
      title: title.length == 0
          ? null
          : FittedBox(
              fit: BoxFit.contain,
              child: Text(
                title,
                style: GoogleFonts.dancingScript(
                  textStyle: TextStyle(
                    fontSize: 60,
                    color: Color.fromRGBO(212, 195, 176, 1),
                  ),
                ),
              ),
            ),

      /// mapowanie labeli na obiekty IconButton z przekierowaniami na inne podstrony
      actions: [
        // ... -> destrukturyzja (wyciągniecie wszystkich elementów z listy w navbarze)
        ...labels
            .map((l) => IconButton(
                icon: Icon(
                  l['icon'] as IconData,
                  color: Color.fromRGBO(212, 195, 176, 1),
                ),
                // zamień aktualnego routa (aktualną podstronę) na nową z routeName
                onPressed: () => Navigator.of(context).pushReplacementNamed(
                    l['routeName'] as String,
                    arguments: l['arguments'])))
            .toList(),
        // dorzucenie searcha
        if (id == 2) ...actions
      ]);
}
