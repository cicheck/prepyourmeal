import 'package:cookbook/models/recipe_model.dart';
import 'package:cookbook/widgets/recipe_list_item.dart';
import 'package:flutter/material.dart';

class RecipesList extends StatelessWidget {
  final List<RecipeModel> recipes;
  // super - wywołaj konstruktor StatelessWidgeta, który na wejście bierze key
  const RecipesList({Key? key, required this.recipes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromRGBO(212, 195, 176, 1),
      child: ListView.builder(
          padding: const EdgeInsets.all(12),
          itemCount: this.recipes.length,
          itemBuilder: (BuildContext context, int index) {
            return RecipeListItem(
              recipe: this.recipes[index],
            );
          }),
    );
  }
}
