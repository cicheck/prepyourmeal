import 'package:flutter/material.dart';

/// Widget pojedynczej tabelki z menu na stronie głównej
class MenuLabel extends StatelessWidget {
  final IconData icon;
  final String title;
  final String routeName;
  final int id;
  final String arguments;

  MenuLabel(
      {required this.icon,
      required this.title,
      required this.routeName,
      required this.id,
      this.arguments = ""});

  @override
  Widget build(BuildContext context) {
    Color tileColor = this.id == 1
        ? Color.fromRGBO(41, 36, 33, .95)
        : Color.fromRGBO(212, 205, 197, .9);
    Color textColor = this.id == 1
        ? Color.fromRGBO(212, 205, 197, 1)
        : Color.fromRGBO(41, 36, 33, 1);
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: tileColor,
      ),
      child: ListTile(
        onTap: () {
          Navigator.pushNamed(context, this.routeName,
              arguments: this.arguments);
        },
        leading: Icon(this.icon, size: 25, color: textColor),
        title: Text(this.title,
            style: TextStyle(
              color: textColor,
              fontSize: 20,
            )),
      ),
    );
  }
}
