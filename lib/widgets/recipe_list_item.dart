import 'package:cookbook/models/recipe_model.dart';
import 'package:cookbook/providers/recipes_provider.dart';
import 'package:cookbook/screens/recipe_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// pojedynczy przepis w liście przepisów */
class RecipeListItem extends StatelessWidget {
  final RecipeModel recipe;
  RecipeListItem({required this.recipe});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color.fromRGBO(241, 233, 227, 1),
      // InkWell po to, żeby mieć interakcje typu onTap
      child: InkWell(
        splashColor: Colors.blue.withAlpha(30),
        onTap: () {
          Provider.of<RecipesProvider>(context, listen: false)
              .addRecipeToRecents(this.recipe);
          // dynamiczne tworzenie strony z przepisem
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RecipeScreen(recipeModel: this.recipe)),
          );
        },
        child: ListTile(
          dense: true,
          title: Text(recipe.title),
          contentPadding:
              EdgeInsets.only(bottom: 0, top: 0, left: 0, right: 15),
          // leading - max na lewo
          leading: Container(
            width: 100,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4), bottomLeft: Radius.circular(5)),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: recipe.image.image,
              ),
            ),
          ),
          // trailing - max na prawo
          trailing: Text('${recipe.minutes} min'),
        ),
      ),
    );
  }
}
