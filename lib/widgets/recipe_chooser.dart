import 'package:cookbook/models/recipe_model.dart';
import 'package:flutter/material.dart';

/// DropdownButton do wybierania przepisu w danym dniu w kalendarzu */
// ignore: must_be_immutable
class RecipeChooser extends StatefulWidget {
  late final List items;
  late RecipeModel dropdownValue;
  late final Function callback;
  late final Function addRecipeCallback;
  RecipeChooser(
      {Key? key,
      required this.items,
      required this.callback,
      required this.addRecipeCallback})
      : super(key: key) {
    this.dropdownValue = this.items[0];
  }

  @override
  _RecipeChooserState createState() => _RecipeChooserState();
}

class _RecipeChooserState extends State<RecipeChooser> {
  TextEditingController weightController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            DropdownButton<RecipeModel>(
                value: widget.dropdownValue,
                icon: const Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                // style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 3,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color.fromRGBO(79, 73, 65, 1),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 4,
                            offset: Offset(0, 2),
                            spreadRadius: 0,
                            color: Color.fromRGBO(0, 0, 0, .15))
                      ]),
                ),
                onChanged: (RecipeModel? newValue) {
                  setState(() {
                    widget.dropdownValue = newValue!;
                    widget.callback(
                      newValue,
                    );
                  });
                },
                items:
                    widget.items.map<DropdownMenuItem<RecipeModel>>((product) {
                  return DropdownMenuItem<RecipeModel>(
                    value: product,
                    child: Text(product.title),
                  );
                }).toList()),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: ElevatedButton(
                child: const Text("Add recipe"),
                onPressed: () {
                  this.widget.addRecipeCallback();
                  this.weightController.clear();
                },
              ),
            ),
          ],
        ));
  }
}
