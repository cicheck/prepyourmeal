import 'package:cookbook/models/ProductChooserModel.dart';
import 'package:cookbook/models/product_model.dart';
import 'package:flutter/material.dart';

/// DropdownButton - wybieranie produktu z listy przy dodawaniu nowego przepisu */
// ignore: must_be_immutable
class ProductChooser extends StatefulWidget {
  late final List items;
  late ProductModel dropdownValue;
  late final Function(ProductChooserModel) callback;
  late final Function addProductCallback;
  ProductChooser(
      {Key? key,
      required this.items,
      required this.callback,
      required this.addProductCallback})
      : super(key: key) {
    this.dropdownValue = this.items[0];
  }

  @override
  _ProductChooserState createState() => _ProductChooserState();
}

class _ProductChooserState extends State<ProductChooser> {
  // wpisywanie gram
  TextEditingController weightController = TextEditingController();
  // wartość początkowa gram
  String initialValue = "1";
  @override
  Widget build(BuildContext context) {
    weightController.text = initialValue;
    return Padding(
        padding: EdgeInsets.all(0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(children: [
              // równy podział wiersza na 2 części (bo mamy 2 expanded)
              Expanded(
                child: Card(
                  elevation: 2,
                  margin: EdgeInsets.only(bottom: 2, right: 15),
                  color: Color.fromRGBO(241, 233, 227, 1),
                  child: TextField(
                    controller: weightController,
                    onChanged: (e) {
                      widget.callback(ProductChooserModel(
                        this.widget.dropdownValue,
                        int.parse(weightController.text),
                      ));
                    },
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      labelText: 'how much',
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ),
              Expanded(
                child: DropdownButton<ProductModel>(
                    value: widget.dropdownValue,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    // style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 3,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color.fromRGBO(79, 73, 65, 1),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                offset: Offset(0, 2),
                                spreadRadius: 0,
                                color: Color.fromRGBO(0, 0, 0, .15))
                          ]),
                    ),
                    onChanged: (ProductModel? newValue) {
                      setState(() {
                        widget.dropdownValue = newValue!;
                        initialValue = weightController.text;
                        widget.callback(ProductChooserModel(
                          newValue,
                          int.parse(weightController.text),
                        ));
                      });
                    },
                    items: widget.items
                        .map<DropdownMenuItem<ProductModel>>((product) {
                      // DropdownButton oczekuje listy DropdownMenuItem<Object>, w naszym przypadku Object to ProductModel
                      // z tego powodu mapujemy listę produktów na Widgety DropdownMenuItem<ProductModel>
                      return DropdownMenuItem<ProductModel>(
                        value: product,
                        child: Text(product.title),
                      );
                    }).toList()),
              ),
            ]),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: ElevatedButton(
                child: const Text("Add product"),
                onPressed: () {
                  this.widget.addProductCallback();
                  this.weightController.clear();
                  initialValue = "1";
                },
              ),
            ),
          ],
        ));
  }
}
