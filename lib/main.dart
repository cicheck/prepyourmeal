import 'package:cookbook/providers/recipes_provider.dart';
import 'package:cookbook/screens/add_recipe_screen.dart';
import 'package:cookbook/screens/meal_planner_screen.dart';
import 'package:cookbook/screens/search_recipe_screen.dart';
import 'package:cookbook/widgets/menu_label.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'screens/recipes_list_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp()); // odpalenie aplikacji
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    // ustawienie głównego koloru
    Map<int, Color> color = {
      50: Color.fromRGBO(46, 42, 38, .1),
      100: Color.fromRGBO(46, 42, 38, .2),
      200: Color.fromRGBO(46, 42, 38, .3),
      300: Color.fromRGBO(46, 42, 38, .4),
      400: Color.fromRGBO(46, 42, 38, .5),
      500: Color.fromRGBO(46, 42, 38, .6),
      600: Color.fromRGBO(46, 42, 38, .7),
      700: Color.fromRGBO(46, 42, 38, .8),
      800: Color.fromRGBO(46, 42, 38, .9),
      900: Color.fromRGBO(46, 42, 38, 1),
    };
    // kolor aplikacji
    MaterialColor myColour = MaterialColor(0xFF2E2A26, color);

    return MultiProvider(
        /*
        provider - odpowiada za główny stan aplikacji, musimy podać providera i wszystkie klasy, które będą z niego korzystać 
        */
        providers: [
          // podajemy providera, ChangeNotifierProvider może powiadomić swoich nasłuchujących o tym, że zaszła jakaś zmniana
          // poprzez funkcję notifyListeners()
          ChangeNotifierProvider<RecipesProvider>(
            create: (context) => RecipesProvider(),
          ),
        ],
        child: MaterialApp(
          // aplikacja
          title: 'CookBook',
          theme: ThemeData(
            primarySwatch: myColour,
            accentColor: myColour,
          ),
          home: MyHomePage(
              title:
                  'CookBook HomePage'), // strona główna, tutaj wchodzimy po wejściu w aplkację
          routes: {
            AddRecipeScreen.routeName: (context) =>
                AddRecipeScreen(), // podstrony, mamy routes, czyli jak w webie, np. tutaj mamy app/addRecipe
            RecipesListScreen.routeName: (context) => RecipesListScreen(),
            SearchRecipeScreen.routeName: (context) => SearchRecipeScreen(),
            MealPlannerScreen.routeName: (context) => MealPlannerScreen(),
          },
        ));
  }
}

class MyHomePage extends StatelessWidget {
  // stateless to znaczy, że nie ma stanu, czyli nie będzie się rebuildować
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  final labels = [
    // nasze menu
    {
      "title": "Add Recipe",
      "icon": (Icons.add),
      "routeName": AddRecipeScreen.routeName,
      "id": 1,
    },
    {
      "title": "Search recipes",
      "icon": (Icons.search),
      "routeName": SearchRecipeScreen.routeName,
      "id": 2,
    },
    {
      "title": "My recipes",
      "icon": (Icons.menu_book),
      "routeName": RecipesListScreen.routeName,
      "id": 3,
      "arguments": RecipesListScreen.myRecipes,
    },
    {
      "title": "Recent Recipes",
      "icon": (Icons.schedule_outlined),
      "routeName": RecipesListScreen.routeName,
      "id": 4,
      "arguments": RecipesListScreen.recentRecipes,
    },
    {
      "title": "Meal planner",
      "icon": (Icons.calendar_today),
      "routeName": MealPlannerScreen.routeName,
      "id": 5,
    },
  ];

  @override
  Widget build(BuildContext context) {
    // build - tutaj mamy tworzenie wyglądu widgeta
    var provider = Provider.of<RecipesProvider>(context, listen: false);
    // uruchamiamy provider, żeby załadować się na stronie głównej
    return Scaffold(
      backgroundColor: Color.fromRGBO(212, 195, 176, 1),
      body: Container(
        // podstawowy widget, kontener
        decoration: BoxDecoration(
          // tutaj dajemy obrazek jako tło kontenera
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          // stos - jedno po drugim ustawiamy
          children: [
            Container(
                margin: EdgeInsets.only(top: 30),
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "CookBook",
                      style: GoogleFonts.dancingScript(
                        textStyle: TextStyle(
                          fontSize: 60,
                          color: Color.fromRGBO(212, 195, 176, 1),
                        ),
                      ),
                    )
                  ],
                )),
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: labels
                    .map((l) => MenuLabel(
                          icon: l['icon'] as IconData,
                          routeName: l['routeName'] as String,
                          title: l['title'] as String,
                          id: l['id'] as int,
                          arguments: (l.containsKey('arguments')
                              ? l['arguments']
                              : '') as String,
                        ))
                    .toList()),
          ],
        ),
      ),
    );
  }
}
