import 'package:cookbook/models/product_model.dart';
import 'package:flutter/widgets.dart';
import 'dart:convert';
import 'ProductChooserModel.dart';

/// obiekt przepisu */
class RecipeModel {
  final int id;
  final String title;
  final Image image;
  final List<ProductChooserModel> products;
  final int portions;
  final int minutes;
  final List<String> steps;

  /// required, bo zawsze wymagane i jak pola są final, to musi być required
  RecipeModel({
    required this.id,
    required this.title,
    required this.image,
    required this.products,
    required this.portions,
    required this.minutes,
    required this.steps,
  });

  toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'image': (this.image.image as NetworkImage).url,
      // encode to zamiana na json
      'products': jsonEncode(this
          .products
          .map((product) => ({
                'grams': product.grams,
                'product': product.product.toMap(),
              }))
          .toList()),
      'portions': this.portions,
      'minutes': this.minutes,
      'steps': jsonEncode(this.steps),
    };
  }

  RecipeModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        image = Image.network(json['image']),
        // decode - z jsona na zwykły obiekt
        steps = (jsonDecode(json['steps']) as List<dynamic>).cast<String>(),
        portions = json['portions'],
        minutes = json['minutes'],
        products = (jsonDecode(json['products']) as List<dynamic>)
            .map((prod) => ProductChooserModel(
                ProductModel(
                  id: prod['product']['id'],
                  title: prod['product']['title'],
                  carbohydrates: prod['product']['carbohydrates'],
                  fat: prod['product']['fat'],
                  protein: prod['product']['protein'],
                ),
                prod['grams']))
            .toList();
}
