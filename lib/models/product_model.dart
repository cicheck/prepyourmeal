/// obiekt produktu */
class ProductModel {
  final int id;
  final String title;
  final int protein;
  final int fat;
  final int carbohydrates;
  int get calories => protein * 4 + fat * 7 + carbohydrates * 4;
  ProductModel({
    required this.id,
    required this.protein,
    required this.fat,
    required this.carbohydrates,
    required this.title,
  });

  /// zmiana produktu na obiekt json w celu dodania go do bazy sqlite */
  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'protein': this.protein,
      'fat': this.fat,
      'carbohydrates': this.carbohydrates,
    };
  }
}
