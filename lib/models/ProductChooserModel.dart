import 'package:cookbook/models/product_model.dart';

/// obiekt produktu z jego gramaturgią */
class ProductChooserModel {
  final ProductModel product;
  final int grams;

  ProductChooserModel(this.product, this.grams);
}
