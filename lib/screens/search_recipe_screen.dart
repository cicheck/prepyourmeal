import 'package:cookbook/providers/recipes_provider.dart';
import 'package:cookbook/widgets/app_bar.dart';
import 'package:cookbook/widgets/recipes_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:provider/provider.dart';

/// screen do wyszukiwania przepisów */
class SearchRecipeScreen extends StatefulWidget {
  SearchRecipeScreen({Key? key}) : super(key: key);
  static const routeName = "searchRecipes";
  @override
  _SearchRecipeScreenState createState() => new _SearchRecipeScreenState();
}

class _SearchRecipeScreenState extends State<SearchRecipeScreen> {
  // searchBar to zewnętrzna bilbioteka
  late SearchBar searchBar;
  String searchValue = "";

  AppBar buildAppBar(BuildContext context) {
    return createAppBar(
        title: 'CookBook',
        context: context,
        id: 2,
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    setState(() {
      searchValue = value;
    });
  }

  _SearchRecipeScreenState() {
    searchBar = new SearchBar(
        //inBar - nie bierz domyślnych kolorów aplikacji
        inBar: false,
        // nasza funkcja tworząca appbar
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted);
  }

  @override
  Widget build(BuildContext context) {
    // wywołanie z providera funkcji searchRecipes
    var searchedRecipes = Provider.of<RecipesProvider>(context, listen: false)
        .searchRecipes(searchValue);

    return new Scaffold(
        // appBar jest budowany przez SearchBar na podstawie naszej funkcji "buildAppBar", gdzie searchBar do tego podczepia swoją wyszukiwarkę
        appBar: searchBar.build(context),
        body: RecipesList(recipes: searchedRecipes));
  }
}
