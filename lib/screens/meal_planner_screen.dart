import 'dart:collection';
import 'package:cookbook/widgets/app_bar.dart';
import 'package:intl/intl.dart';

import 'package:cookbook/models/recipe_model.dart';
import 'package:cookbook/providers/recipes_provider.dart';
import 'package:cookbook/utils/calendar_util.dart';
import 'package:cookbook/widgets/recipe_chooser.dart';
import 'package:cookbook/widgets/recipe_list_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class MealPlannerScreen extends StatefulWidget {
  static final routeName = '/mealPlannerScreen';
  @override
  _MealPlannerScreenState createState() => _MealPlannerScreenState();
}

class _MealPlannerScreenState extends State<MealPlannerScreen> {
  /// lista modeli przepisów danego dnia (czyli np. zmieniamy dzień w kalendarzu, to zmieniamy listę na listę przepisów nowego dnia )
  late ValueNotifier<List<RecipeModel>> _selectedEvents;

  CalendarFormat _calendarFormat = CalendarFormat.month;

  /// możemy wybrać jakiś okres
  RangeSelectionMode _rangeSelectionMode = RangeSelectionMode
      .toggledOff; // Can be toggled on/off by longpressing a date
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  DateTime? _rangeStart;
  DateTime? _rangeEnd;
  LinkedHashMap<DateTime, List<RecipeModel>> _items =
      new LinkedHashMap<DateTime, List<RecipeModel>>();

  @override
  void initState() {
    super.initState();
    _selectedDay = _focusedDay;
    _selectedEvents = ValueNotifier(_getEventsForDay(_selectedDay!));
  }

  @override
  void dispose() {
    /// jak kalendarz się usuwa, to musimy usunąć też _selectedEvents
    _selectedEvents.dispose();
    super.dispose();
  }

  RecipeModel? dropdownValue;
  void changeDropdownValue(RecipeModel newDropdownValue) {
    dropdownValue = newDropdownValue;
  }

  void addRecipe(provider) {
    setState(() {
      String dayString = DateFormat("dd-MM-yyyy").format(_selectedDay!);
      DateTime day = DateFormat("dd-MM-yyyy").parse(dayString);
      provider.addCalendarMapItem(day, dropdownValue!);
      _selectedEvents = ValueNotifier(_getEventsForDay(day));
      _selectedDay = _focusedDay;
    });
  }

  /// zwraca listę przepisów w danym dniu lub pustą listę, jeżeli nie ma
  List<RecipeModel> _getEventsForDay(DateTime day) {
    return _items[day] ?? [];
  }

  List<RecipeModel> _getEventsForRange(DateTime start, DateTime end) {
    final days = daysInRange(start, end);
    return [
      for (final d in days) ..._getEventsForDay(d),
    ];
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    if (!isSameDay(_selectedDay, selectedDay)) {
      setState(() {
        _selectedDay = selectedDay;
        _focusedDay = focusedDay;
        _rangeStart = null; // Important to clean those
        _rangeEnd = null;
        _rangeSelectionMode = RangeSelectionMode.toggledOff;
      });

      _selectedEvents.value = _getEventsForDay(selectedDay);
    }
  }

  void _onRangeSelected(DateTime? start, DateTime? end, DateTime focusedDay) {
    setState(() {
      _selectedDay = null;
      _focusedDay = focusedDay;
      _rangeStart = start;
      _rangeEnd = end;
      _rangeSelectionMode = RangeSelectionMode.toggledOn;
    });

    if (start != null && end != null) {
      _selectedEvents.value = _getEventsForRange(start, end);
    } else if (start != null) {
      _selectedEvents.value = _getEventsForDay(start);
    } else if (end != null) {
      _selectedEvents.value = _getEventsForDay(end);
    }
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RecipesProvider>(
      context,
      listen: true,
    );

    /// pobieranie listy wszystkich przepisów na wszystkie dni z providera
    _items = provider.getCalendarMap();

    /// ustawiamy dropdownButton na początkową wartość równą pierwszemu przepisowi na liście w providerze
    provider.getRecipes().then((value) => dropdownValue = value[0]);
    return Scaffold(
      appBar: createAppBar(title: 'CookBook', context: context, id: 5),
      body: Column(
        children: [
          TableCalendar<RecipeModel>(
            calendarBuilders: CalendarBuilders(
              /// ustawienie, że dzień dzisiejszy to brązowe kółko z białymi napisami
              todayBuilder: (context, day, focusedDay) => Container(
                  width: double.infinity * .85,
                  height: double.infinity * .85,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromRGBO(46, 42, 38, 1),
                  ),
                  child: Center(
                      child: Text(
                    '${day.day}',
                    style: TextStyle(color: Colors.white),
                  ))),

              /// analogicznie do todayBuilder, tylko tutaj kremowe kółko
              selectedBuilder: (context, day, focusedDay) => Container(
                  width: double.infinity * .85,
                  height: double.infinity * .85,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Color.fromRGBO(241, 233, 227, 1),
                  ),
                  child: Center(child: Text('${day.day}'))),
            ),

            /// pobieranie pierwszego i ostatniego dnia kalendarza z utlisów
            firstDay: kFirstDay,
            lastDay: kLastDay,
            focusedDay: _focusedDay,

            /// funkcja, która mówi, czy dany dzień powinien być ustawiony na selected
            selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
            rangeStartDay: _rangeStart,
            rangeEndDay: _rangeEnd,

            /// miesiąc / 2 tygodnie / tydzień
            calendarFormat: _calendarFormat,
            rangeSelectionMode: _rangeSelectionMode,
            eventLoader: _getEventsForDay,
            startingDayOfWeek: StartingDayOfWeek.monday,
            calendarStyle: CalendarStyle(
              // Use `CalendarStyle` to customize the UI
              outsideDaysVisible: false,
            ),
            onDaySelected: _onDaySelected,
            onRangeSelected: _onRangeSelected,
            onFormatChanged: (format) {
              if (_calendarFormat != format) {
                setState(() {
                  _calendarFormat = format;
                });
              }
            },
            onPageChanged: (focusedDay) {
              _focusedDay = focusedDay;
            },
          ),
          const SizedBox(height: 8.0),
          Expanded(
            child: ValueListenableBuilder<List<RecipeModel>>(
              valueListenable: _selectedEvents,
              builder: (context, value, _) {
                return ListView.builder(
                  itemCount: value.length + 1,
                  itemBuilder: (context, index) {
                    if (index < value.length) {
                      return RecipeListItem(
                        recipe: value[index],
                      );
                    } else {
                      return RecipeChooser(
                        items: provider.recipes,
                        callback: changeDropdownValue,
                        addRecipeCallback: () => {addRecipe(provider)},
                      );
                    }
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
