import 'dart:math';

import 'package:cookbook/models/ProductChooserModel.dart';
import 'package:cookbook/models/recipe_model.dart';
import 'package:cookbook/providers/recipes_provider.dart';
import 'package:cookbook/widgets/app_bar.dart';
import 'package:cookbook/widgets/product_chooser.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transparent_image/transparent_image.dart';

class AddRecipeScreen extends StatefulWidget {
  static final routeName = '/addRecipe';

  @override
  _AddRecipeScreenState createState() => _AddRecipeScreenState();
}

class _AddRecipeScreenState extends State<AddRecipeScreen> {
  String _imageUrl =
      "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/13385.jpg?output-format=auto&output-quality=auto";
  List<String> steps = [];
  List<ProductChooserModel> products = [];
  int stepsCount = 0;
  int grams = 0;
  ProductChooserModel? dropdownValue;
  void changeImageUrl(String newImageUrl) {
    setState(() {
      _imageUrl = newImageUrl;
    });
  }

  void addStep() {
    setState(() {
      steps.add(stepController.text);
      stepsCount++;
      stepController.clear();
    });
  }

  void changeDropdownValue(ProductChooserModel newDropdownValue) {
    dropdownValue = newDropdownValue;
  }

  void addProduct() {
    setState(() {
      products.add(dropdownValue!);
    });
  }

  // Widget TextEdit potrzebuje controllera, który przechowuje i aktualizuje wyświetlany tekst
  TextEditingController portionsController = TextEditingController();
  TextEditingController titleController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController imageController = TextEditingController();
  TextEditingController stepController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RecipesProvider>(
      context,
      listen: true,
    );
    dropdownValue = ProductChooserModel(provider.products[0], 0);
    return Scaffold(
      appBar: createAppBar(title: 'CookBook', context: context, id: 1),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        color: Color.fromRGBO(212, 195, 176, 1),
        child: ListView(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 8),
            child: Card(
              margin: EdgeInsets.all(1),
              elevation: 2,
              color: Color.fromRGBO(241, 233, 227, 1),
              child: TextField(
                controller: titleController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  labelText: 'Recipe Title',
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                ),
              ),
            ),
          ),
          // ClipRRect daje możliwość dodania border radius do obrazka
          ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: _imageUrl,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8, top: 8),
            child: Card(
              margin: EdgeInsets.all(1),
              elevation: 2,
              color: Color.fromRGBO(241, 233, 227, 1),
              child: TextField(
                controller: imageController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  labelText: 'Image',
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                ),
                onEditingComplete: () {
                  changeImageUrl(imageController.text);
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8),
            child: Card(
              margin: EdgeInsets.all(1),
              elevation: 2,
              color: Color.fromRGBO(241, 233, 227, 1),
              child: TextField(
                controller: portionsController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  labelText: 'Number of Portions',
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                ),
                keyboardType: TextInputType.number,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8),
            child: Card(
              margin: EdgeInsets.all(1),
              elevation: 2,
              color: Color.fromRGBO(241, 233, 227, 1),
              child: TextField(
                controller: timeController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  labelText: 'Time',
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                ),
                keyboardType: TextInputType.number,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 1, right: 1, bottom: 8),
            child: const Text(
              'Steps',
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8),
            child: Card(
              margin: EdgeInsets.all(1),
              color: Color.fromRGBO(241, 233, 227, 1),
              elevation: 2,
              child: TextField(
                controller: stepController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  labelText: 'Add step',
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                ),
              ),
            ),
          ),
          ElevatedButton(
            child: const Text("Add step"),
            onPressed: () {
              addStep();
            },
          ),
          Container(
            // żeby w pewnym momencie lista stepów była scrollowana
            height: min(150, (this.stepsCount) * 75.0),
            // double.infinity -> zajmij całą wolną przestrzeń
            width: double.infinity,
            child: ListView.builder(
                padding: EdgeInsets.only(bottom: 8),
                itemCount: stepsCount,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                      color: Color.fromRGBO(241, 233, 227, 1),
                      child: ListTile(
                          dense: true,
                          title: Text(
                            '${index + 1}. ${steps[index]}',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 42, 38, .65)),
                          )));
                }),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8, left: 1, right: 1),
            child: const Text(
              'Products',
              style: TextStyle(fontSize: 20),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: ProductChooser(
              items: provider.products,
              callback: changeDropdownValue,
              addProductCallback: addProduct,
            ),
          ),
          Container(
            height: min(150, (this.products.length) * 75.0),
            width: double.infinity,
            child: ListView.builder(
                padding: EdgeInsets.only(bottom: 8),
                itemCount: products.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    color: Color.fromRGBO(241, 233, 227, 1),
                    child: ListTile(
                      dense: true,
                      title: Text(
                        '${index + 1}. ${products[index].product.title}',
                        style: TextStyle(
                          color: Color.fromRGBO(46, 42, 38, .65),
                        ),
                      ),
                      trailing: Text(
                        '${products[index].grams} grams',
                        style: TextStyle(
                          color: Color.fromRGBO(46, 42, 38, .65),
                        ),
                      ),
                    ),
                  );
                }),
          ),
          ElevatedButton(
            child: const Text("Add recipe"),
            onPressed: () {
              var provider =
                  Provider.of<RecipesProvider>(context, listen: false);
              provider.addRecipe(RecipeModel(
                id: provider.recipes.length + 1,
                image: Image.network(_imageUrl),
                minutes: int.parse(timeController.text),
                portions: int.parse(portionsController.text),
                products: products,
                steps: steps,
                title: titleController.text,
              ));
              // programistyczne cofnięcie się do poprzedniego screena
              Navigator.of(context).pop();
            },
          ),
        ]),
      ),
    );
  }
}
