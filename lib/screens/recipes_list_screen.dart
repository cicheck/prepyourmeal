import 'package:cookbook/providers/recipes_provider.dart';
import 'package:cookbook/widgets/app_bar.dart';
import 'package:cookbook/widgets/recipes_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// screen z przepisami */
class RecipesListScreen extends StatefulWidget {
  static const routeName = '/recipes-list';
  static const myRecipes = 'My recipes';
  static const recentRecipes = 'Recent recipes';

  @override
  _RecipesListScreenState createState() => _RecipesListScreenState();
}

class _RecipesListScreenState extends State<RecipesListScreen> {
  @override
  Widget build(BuildContext context) {
    // do tego screena przechodzimy przez routeName, tutaj mamy wyciąganie argumentów podanych przez pushNamed
    final type = ModalRoute.of(context)!.settings.arguments;
    var provider = Provider.of<RecipesProvider>(
      context,
      listen: true,
    );
    var recipes = type == RecipesListScreen.myRecipes
        ? provider.recipes
        : provider.recentRecipes;

    var id = type == RecipesListScreen.myRecipes ? 3 : 4;

    return Scaffold(
        appBar: createAppBar(
          title: 'CookBook',
          context: context,
          id: id,
        ),
        body: Container(
          child: RecipesList(recipes: recipes),
        ));
  }
}
