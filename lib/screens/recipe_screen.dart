import 'dart:math';

import 'package:cookbook/models/recipe_model.dart';
import 'package:cookbook/widgets/app_bar.dart';
import 'package:flutter/material.dart';

class RecipeScreen extends StatelessWidget {
  final RecipeModel? recipeModel;
  RecipeScreen({required this.recipeModel});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: createAppBar(title: 'CookBook', context: context, id: 0),
      backgroundColor: Color.fromRGBO(212, 195, 176, 1),
      body: ListView(children: <Widget>[
        Padding(
          padding: EdgeInsets.all(15),
          child: Center(
            child: Text(
              recipeModel!.title,
              style: TextStyle(
                fontSize: 24,
                color: Color.fromRGBO(46, 42, 38, 1),
              ),
            ),
          ),
        ),
        recipeModel!.image,
        Card(
            color: Color.fromRGBO(241, 233, 227, 1),
            margin: EdgeInsets.all(10),
            elevation: 4,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: RichText(
                        text: TextSpan(children: [
                      WidgetSpan(
                        child: Icon(
                          Icons.restaurant_menu,
                          size: 24,
                          color: Color.fromRGBO(46, 42, 38, .5),
                        ),
                        alignment: PlaceholderAlignment.middle,
                      ),
                      WidgetSpan(
                          child: Padding(
                        padding: EdgeInsets.all(2),
                      )),
                      TextSpan(
                        text: "${recipeModel!.portions} portions",
                        style: TextStyle(
                            fontSize: 16, color: Color.fromRGBO(46, 42, 38, 1)),
                      ),
                    ])),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: RichText(
                        text: TextSpan(children: [
                      WidgetSpan(
                        child: Icon(
                          Icons.schedule,
                          size: 24,
                          color: Color.fromRGBO(46, 42, 38, .5),
                        ),
                        alignment: PlaceholderAlignment.middle,
                      ),
                      WidgetSpan(
                          child: Padding(
                        padding: EdgeInsets.all(2),
                      )),
                      TextSpan(
                        text: "${recipeModel!.minutes} minutes",
                        style: TextStyle(
                            fontSize: 16, color: Color.fromRGBO(46, 42, 38, 1)),
                      ),
                    ])),
                  )
                ])),
        Padding(
          padding: EdgeInsets.all(8),
          child: const Text("Steps:"),
        ),
        Container(
          height: min(150, (recipeModel!.steps.length) * 75.0),
          width: double.infinity,
          child: ListView.builder(
              padding: EdgeInsets.only(bottom: 8),
              itemCount: recipeModel!.steps.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    color: Color.fromRGBO(241, 233, 227, 1),
                    child: ListTile(
                        dense: true,
                        title: Text(
                          '$index. ${recipeModel!.steps[index]}',
                          style:
                              TextStyle(color: Color.fromRGBO(46, 42, 38, .65)),
                        )));
              }),
        ),
        Padding(
          padding: EdgeInsets.all(8),
          child: const Text("Products:"),
        ),
        Container(
          height: min(150, (recipeModel!.products.length) * 75.0),
          width: double.infinity,
          child: ListView.builder(
              padding: EdgeInsets.only(bottom: 8),
              itemCount: recipeModel!.products.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  color: Color.fromRGBO(241, 233, 227, 1),
                  child: ListTile(
                    dense: true,
                    title: Text(
                      '${index + 1}. ${recipeModel!.products[index].product.title}',
                      style: TextStyle(
                        color: Color.fromRGBO(46, 42, 38, .65),
                      ),
                    ),
                    trailing: Text(
                      '${recipeModel!.products[index].grams} grams',
                      style: TextStyle(
                        color: Color.fromRGBO(46, 42, 38, .65),
                      ),
                    ),
                  ),
                );
              }),
        ),
      ]),
    );
  }
}
